/**
 * This class represents a cat.
 * 
 * Cat should inherit from AAnimal.
 * 
 * Cat should override the methods necessary to make this a concrete class.
 * 
 * A cats happiness improves only if it is fed cat food.
 * 
 * @author bricks
 *
 */

public class Cat extends AAnimal {
	
	/**
	 * @param food the type of food being given to the cat
	 */
	@Override
	public void feed(IFood food) {
		if (food instanceof CatFood) {
			super.improveHappiness();
		}
	}


}
